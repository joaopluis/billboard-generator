import Vue from 'vue';
import VueI18n from 'vue-i18n'
import VueRouter from 'vue-router'
import VueAnalytics from 'vue-analytics'

const bowser = require('bowser');

Vue.use(VueI18n);
Vue.use(VueRouter);

import Index from './views/Index.vue'
import About from './views/About.vue'
import StoreBillboard from './views/store-billboard/StoreBillboard.vue'
import RideBillboard from './views/ride-billboard/RideBillboard.vue'

// Create VueI18n instance with options
const i18n = new VueI18n({
    locale: 'en', // set locale
});

const routes = [
    { path: '/', component: Index },
    { path: '/about', component: About },
    { path: '/ride-billboard', component: RideBillboard },
    { path: '/store-billboard', component: StoreBillboard }
];

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
    routes // short for `routes: routes`
});

Vue.use(VueAnalytics, {
    id: process.env.ANALYTICS_ID,
    router
});

new Vue({
    el: '#app',
    computed: {
        browser () {
            return bowser
        }
    },
    router,
    i18n
});