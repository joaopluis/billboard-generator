export default [
  {
    "name": "Chief Beef",
    "color": "#0E6DB1",
    "logos": [
      require("./images/store-logos/chief-beef.png"), require("./images/store-logos/chief-beef-land.png")
    ],
    "bg": require("./images/store-photos/chief-beef.png"),
    "bbImages": [require("./images/store-icons/burger.png")],
    "items": [
      {
        "item": "Menu",
        "value": null
      },
      {
        "item": "Chief Burger XL",
        "value": 12
      },
      {
        "item": "Chief Burger",
        "value": 8
      }
    ]
  },
  {
    "name": "Hotdog Squad",
    "color": "#DB3E04",
    "logos": [
        require("./images/store-logos/hotdog-squad.png"), require("./images/store-logos/hotdog-squad-land.png")
    ],
    "bg": require("./images/store-photos/hotdog-squad.png"),
    "bbImages": [require("./images/store-icons/hotdog.png")],
    "items": [
      {
        "item": "Menu",
        "value": null
      },
      {
        "item": "Jumbo Hotdog",
        "value": 12
      },
      {
        "item": "Hotdog",
        "value": 8
      }
    ]
  },
    {
        "name": "Pizza Pen",
        "color": "#DC571F",
        "logos": [
            require("./images/store-logos/pizza-pen.png")],
        "bg": require("./images/store-photos/pizza-pen.png"),
        "bbImages": [require("./images/store-icons/pizza.png")],
        "items": [
            {
                "item": "Menu",
                "value": null
            },
            {
                "item": "Margherita Pizza Slice",
                "value": 7.5
            },
            {
                "item": "Pepperoni Pizza Slice",
                "value": 8
            },
            {
                "item": "Hawaiian Pizza Slice",
                "value": 8
            },
            {
                "item": "Four Cheese Pizza Slice",
                "value": 8.5
            }
        ]
    },
    {
        "name": "Gulpee Energy",
        "color": "#00c6dd",
        "logos": [
            require("./images/store-logos/gulpee-energy.png"),
            require("./images/store-logos/gulpee-energy-big.png")
        ],
        "bg": require("./images/store-photos/gulpee-energy.png"),
        "bbImages": [require("./images/store-icons/energydrink.png")],
        "items": [
            {
                "item": "Menu",
                "value": null
            },
            {
                "item": "Gulpee Energy",
                "value": 5
            },
            {
                "item": "Gulpee Energy XL",
                "value": 8
            }
        ]
    },
    {
        "name": "Gulpee Slush",
        "color": "#EC407A",
        "logos": [
            require("./images/store-logos/gulpee-slush.png"),
            require("./images/store-logos/gulpee-slush-big.png")
        ],
        "bg": require("./images/store-photos/gulpee-slush.png"),
        "bbImages": [require("./images/store-icons/slush.png")],
        "items": [
            {
                "item": "Menu",
                "value": null
            },
            {
                "item": "Pink Slush",
                "value": 6.5
            },
            {
                "item": "Blue Slush",
                "value": 6.5
            },
            {
                "item": "Orange Slush",
                "value": 6.5
            },
            {
                "item": "Green Slush",
                "value": 6.5
            }
        ]
    },
    {
        "name": "Gulpee Soda",
        "color": "#F57C00",
        "logos": [
            require("./images/store-logos/gulpee-soda.png")
        ],
        "bg": require("./images/store-photos/gulpee-soda.png"),
        "bbImages": [require("./images/store-icons/soda.png")],
        "items": [
            {
                "item": "Menu",
                "value": null
            },
            {
                "item": "Gulpee",
                "value": 5
            },
            {
                "item": "Gulpee Guava",
                "value": 5
            },
            {
                "item": "Gulpee 0 Cal",
                "value": 5
            },
            {
                "item": "Gulpee Watermelon",
                "value": 5
            }
        ]
    },
    {
        "name": "Street Fox Coffee",
        "color": "#7f1c2e",
        "logos": [
            require("./images/store-logos/street-fox-coffee.png")
        ],
        "bg": require("./images/store-photos/street-fox-coffee.png"),
        "bbImages": [require("./images/store-icons/coffee.png")],
        "items": [
            {
                "item": "Menu",
                "value": null
            },
            {
                "item": "Americano",
                "value": 7
            },
            {
                "item": "Double Espresso",
                "value": 8
            },
            {
                "item": "Cappuccino",
                "value": 7
            },
            {
                "item": "Mocha",
                "value": 7
            },
            {
                "item": "Decaf",
                "value": 6
            }
        ]
    },
    {
        "name": "Justa Momento",
        "color": "#DCB606",
        "logos": [
            require("./images/store-logos/justa-momento.png")
        ],
        "bg": require("./images/store-photos/justa-momento.png"),
        "bbImages": [require("./images/store-icons/bag.png")],
        "items": [
            {
                "item": "Items",
                "value": null
            },
            {
                "item": "Snowglobe",
                "value": 15
            },
            {
                "item": "Crystal Ball",
                "value": 15
            },
            {
                "item": "Sci-Fi Radio",
                "value": 15
            }
        ]
    },
    {
        "name": "Hat's Fantastic",
        "color": "#00ACC1",
        "logos": [
            require("./images/store-logos/hats-fantastic.png")
        ],
        "bg": require("./images/store-photos/hats-fantastic.png"),
        "bbImages": [require("./images/store-icons/hat.png")],
        "items": [
            {
                "item": "Hats",
                "value": null
            },
            {
                "item": "Crown",
                "value": 12
            },
            {
                "item": "Pirate Hat",
                "value": 12
            },
            {
                "item": "Baseball Cap",
                "value": 12
            },
            {
                "item": "Sci-Fi Helmet",
                "value": 12
            },
            {
                "item": "Cowboy Hat",
                "value": 12
            },
            {
                "item": "Archer Hat",
                "value": 12
            }
        ]
    },
    {
        "name": "Loony Blooons",
        "color": "#3FD578",
        "logos": [
            require("./images/store-logos/loony-bloons.png")
        ],
        "bg": require("./images/store-photos/loony-bloons.png"),
        "bbImages": [require("./images/store-icons/baloons.png")],
        "items": [
            {
                "item": "Baloons",
                "value": null
            },
            {
                "item": "Red Balloon",
                "value": 9
            },
            {
                "item": "Orange Baloon",
                "value": 9
            },
            {
                "item": "Green Baloon",
                "value": 9
            },
            {
                "item": "Yellow Baloon",
                "value": 9
            },
            {
                "item": "Pink Baloon",
                "value": 9
            }
        ]
    }
]