const path = require('path');

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const Dotenv = require('dotenv-webpack');

const extractStyles = new ExtractTextPlugin({
    filename: "[name].css",
    disable: process.env.NODE_ENV === "development"
});

function resolve (dir) {
    return path.join(__dirname, 'src', dir)
}

module.exports = {
    entry: ['./src/js/app.js', './src/scss/app.scss', './src/js/fa/packs/regular.js', './src/js/fa/packs/brands.js', './src/js/fa/fontawesome.js'],
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        css: extractStyles.extract({
                            use: "css-loader",
                            fallback: "vue-style-loader"
                        })
                    }
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [resolve('')]
            },
            {
                test: /\.(png|svg|jpe?g|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'images/',
                            name: '[hash].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: extractStyles.extract({
                    use: [
                        "css-loader",
                        "sass-loader"
                    ],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {outputPath: 'fonts/'}
                    }
                ]
            },
            {
                test: /\.html$/,
                use: [
                    'html-loader'
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        extractStyles,
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        new Dotenv()
    ]
};